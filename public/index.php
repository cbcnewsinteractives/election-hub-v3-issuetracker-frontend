<?php
require_once('/cbc/vendor/autoload.php');
$cbc = new \CBC\App();
$slim = $cbc->slim_standard;

$slim->get('/', function($req, $res, $args){
    $templater = $this->templater;
    $template = 'index.twig';
    $variables = [];

    $body = $templater->render($template, $variables);
    return $res->write($body);
});

// Load the viz subpage. This pattern (coupled with the lines in .htaccess)
//  can be used to create your own subpages.
$slim->get('/viz', function($req, $res, $args){
    $templater = $this->templater;
    $template = 'viz.twig';
    $variables = [];

    $body = $templater->render($template, $variables);
    return $res->write($body);
});

$slim->run();

return $slim;
