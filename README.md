# Issue Scrapper

### What's going on here?
Just a working repo to rebuild Issue Tracker with the same codebase as the other election solutions. A work in progress. 

## Now with Discovery Module! - UPDATE 

You may be here to learn how to implement the Discovery Module that will go under the black CBC header on all the Federal Election pages. Let's get started!

This version is live now on [the Party Platforms feature](https://newsinteractives.cbc.ca/elections/federal/2021/party-platforms/) so you can check it out in action. 

### Overview
You don't need to download this repo necessarily - just two new files and six images - and make changes to three additional files so that the module will show up correctly. 

![discovery module](https://lh3.googleusercontent.com/_eug2PZeNxOwoyHlmWkcvcPJMidP1dXwczpR1rkv-FPC5ZV6aV_qY7WWXxL4JB3LmaNwXlLiIQrlItrqnmnL5RCIRM7exjLLBomTK4Kp8mBVz2QX2kCFOyteRhrSh-zuR-pzHpIYCLlRLgHAlDh2RdUjcX9b5j2BJ_bN8GXvJ3sjQAdd6kiv-SaHsWuyG7w_5YMEAUTVHGaFIMCEiqiod_P67TE2n3LvwNTTARtMVCjxHJjcQUkzqfFpjZXuFzw8v1i9Kj8JIXPfzmrA_GDZisCt-6CrJ99H2Ht67OWHBiOgPjFYcFUL0jNgnibg09oY-VYnux_XZNXCIwZyPuaGkAfveP5Z41-UCxrc8NhhzQeb_HooTvbmOt8JHF3YhuNDiAkLXkaUXadCKDsD1z0PZrw7XK9CFoQUAAz74bQyZe_2gh-qfR7c4njmz4UHEd91suc7ZVy4YxEshMkG1xTh4b7pyZVKohHqZ-N9LegK2Z7jLUtlDpxOzfpAtF8KTf8j3eCWeoWpaIX6P5rvU_F0i2jmJmMmt0Na1sxxeBT3cIpLMjPIPIHId-8RzzEUmTA8pR6EO5p1GQuTY1h6GOYH945DAmIdQrujGfUGyt1AE9eKZQ6A45f-NgkzTthVo1zTRhNTSOTrtyQWnHdamde2W5mRYP0LmCzJ_GuvLSgPL3KGbhSrLDAVhrmNeApi81V3QYJOZmfRJOM5iExJ-zfEPMqN=w1153-h126-no?authuser=0)

Changes to files:

[index.twig](https://bitbucket.org/cbcnewsinteractives/election-hub-v3-issuetracker-frontend/src/master/private/app/templates/index.twig) (lines 12 and 13)
[app.js](https://bitbucket.org/cbcnewsinteractives/election-hub-v3-issuetracker-frontend/src/master/private/app/js/app.js) (line 24)
[index.scss](https://bitbucket.org/cbcnewsinteractives/election-hub-v3-issuetracker-frontend/src/master/private/app/styles/index.scss) (lines 207-365)

Entirely new files:

[discovery.js](https://bitbucket.org/cbcnewsinteractives/election-hub-v3-issuetracker-frontend/src/master/private/app/js/scrapper-1.2.0/)
[discovery.twig](https://bitbucket.org/cbcnewsinteractives/election-hub-v3-issuetracker-frontend/src/master/private/app/templates/partials/discovery.twig)

Entirely new images (drop in app/static/images):

[polltracker.png](https://bitbucket.org/cbcnewsinteractives/election-hub-v3-issuetracker-frontend/src/master/private/app/static/images/polltracker.png)
[ask-cbc-news.png](https://bitbucket.org/cbcnewsinteractives/election-hub-v3-issuetracker-frontend/src/master/private/app/static/images/ask-cbc-news.png)
[election-results.png](https://bitbucket.org/cbcnewsinteractives/election-hub-v3-issuetracker-frontend/src/master/private/app/static/images/election-results.png)
[party-platforms.png](https://bitbucket.org/cbcnewsinteractives/election-hub-v3-issuetracker-frontend/src/master/private/app/static/images/party-platforms.png)
[votecompass.png](https://bitbucket.org/cbcnewsinteractives/election-hub-v3-issuetracker-frontend/src/master/private/app/static/images/votecompass.png)
[voter-guide.png](https://bitbucket.org/cbcnewsinteractives/election-hub-v3-issuetracker-frontend/src/master/private/app/static/images/voter-guide.png)
[debate.png](https://bitbucket.org/cbcnewsinteractives/election-hub-v3-issuetracker-frontend/src/master/private/app/static/images/debate.png)


### Installation
Just download the new files and put them in the same spots in your project that they are in this repo.

Then modify your existing three files to include the new lines of code. 

You should now see the new discovery module in your project.

### Setup for Discovery Module

The module was originally designed to promote a whole bunch of stuff that now isn't available, so several links are hidden in index.scss. Depending on which feature you are working on, you will also want to hide certain links. The Party Platforms feature obviously doesn't need a link to itself, so in this file it is hidden in index.scss (example around line 324). You may want to reveal that link and hide others.

After you do that, you have to adjust the width of the discovery module nav ul, so that it adds up to just over the new combined width of all the links you decide to keep. This will prevent overhang when the user scrolls all the way to the right, or the list collapsing under itself. You can find that rule at line 343 of index.scss. 

**Note:** Please double-check the targeting of the links, things might have moved or become Polopoly articles. They can be updated in the discovery.twig file when it comes time to publish.

Any problems? [E-mail Dwight Friesen](mailto:dwight.friesen@cbc.ca?subject=Discovery%20module)

# Scrapper — CBC News Labs starter template



### Setup instructions



* If this your first match with Scrapper, clone the image-builder repository (https://bitbucket.org/cbcnewsinteractives/newsinteractives-docker-image-builder/), and follow instructions to build the base docker images that this repo depends on.

* Fork this project: From the sidebar, click the '+' icon and select 'Fork this repository'; On the next page change Workspace to 'CBC News Interactives', project to 'News Interactives', set a project name, then click the 'Fork Repository' button.

* Once you're forked up, your newe repo will display a command that looks like `git clone https://[your_bitbucket_username]@bitbucket.org/cbcnewsinteractives/[project-name].git` to clone the repository to your machine (or use Sourcetree).

* Head into your newly cloned local repository (`cd /path/to/project-name/`)

* Optional: Set docker-minimal as a secondary remote (so you can easily pull in updates to Scrapper):

```
git remote add scrapper https://[your_bitbucket_username]@bitbucket.org/cbcnewsinteractives/newsinteractives-docker-minimal.git
git fetch scrapper
git branch --track scrapper scrapper/master
```

* Edit cbc.config.dist.yml and set site.urls.web_path appropriately (where it'll be accessed - / is fine initially). Make the same change in .htaccess. Now is also a good time to disable tracking in the config file as well (set `enableTracking` to `false`).

* Edit docker-compose.yml and look for lines labelled CHANGE ME, and set them appropriately

* Commit your configuration changes:
```
git add -A
git commit -m "Configuration changes for new project"
git push origin
```

* Off to the races!

```
docker-compose up
```

* Access your work at http://newsinteractives-dev.cbc.ca/whatever/path/you/set

* Your code lives mostly in `private/` — `private/templates/index.twig` is the entry point for your site.

* Note that your static assets (images, fonts, data, etc) should go in the relevant folders under `private/app/static/`. These will be copied over to the public directory by webpack (anything in the public directory other than index.php is likely to get wiped by webpack so don't store anything in there).


### Deploying for production
Run `bash scripts/build.sh` to generate a deployable index.html & assets/ directory into your public/dist/ directory. These files can be uploaded via FTP or the gcloud command line tool to the newsinteractives server or wherever else you choose to host your site. Note that the web_path (and other variables) in your cbc.config.dist.yml file should be updated to match the production URL structure.

Please refer to the [News Labs launch checklist](https://docs.google.com/document/d/1P77_Lajor3sojKBZdqEbEWAQ0UFEgpivXaYyLj4q3VE/edit?ts=5d07cb24) prior to going live with your project.

### Data sources
One of the advantages of the Twig-based templating system is being able to integrate data sources into your compiled templates without a secondary ajax request. This provides a smoother experience for your users and greatly benefits your page's SEO. As a simple example, you can add a local JSON data source as we did for the [PS752 piece](https://bitbucket.org/cbcnewsinteractives/ps752-memorial/src/master/).

In your `public/index.php` file, after `$variables = [];`:
```
$variables['data'] = json_decode( file_get_contents('/cbc/private/app/static/data/data.json'), true );
```

This points to a JSON file in your `private/app/static/data/` directory. This data will be available to your Twig templates under the `data` variable, which you can render with variables, loops, or one of the many other control structures that Twig provides (see https://twig.symfony.com/doc/3.x/templates.html).

Using similar techniques, you can include data sources from remote URLs, or through e.g. our election CMS.
