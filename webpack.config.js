// baseline...
const path      = require('path');
const webpack   = require('webpack');
const yml       = require('js-yaml');
const fs        = require('fs');
const merge     = require('merge');

// webpack modules..
const TerserPlugin              = require('terser-webpack-plugin');
const MiniCssExtractPlugin      = require("mini-css-extract-plugin");
const OptimizeCSSAssetsPlugin   = require("optimize-css-assets-webpack-plugin");
const HtmlWebpackPlugin         = require('html-webpack-plugin');
const CleanWebpackPlugin        = require('clean-webpack-plugin');
const StyleLintPlugin           = require('stylelint-webpack-plugin');
const CopyPlugin                = require('copy-webpack-plugin');
const FileManagerPlugin         = require('filemanager-webpack-plugin');
const WebpackHookPlugin         = require('webpack-hook-plugin');

// todo: make this a possible environment variable?
const yamldefaults = {
    site: {
        urls: {
            web_path:   '/'
        }
    }
}

const MASTER_CONFIG = 'cbc.config.dist.yml';
const LOCAL_CONFIG  = 'cbc.config.yml';

// read in our config..
function read_config(){
    var to_return = merge.recursive(true,
        yamldefaults,
        fs.existsSync( MASTER_CONFIG ) ? yml.safeLoad(
            // todo: check for empty file/file existince/etc
            fs.readFileSync(MASTER_CONFIG, 'utf8')
        ) : {},
        fs.existsSync( LOCAL_CONFIG ) ? yml.safeLoad(
            fs.readFileSync(LOCAL_CONFIG, 'utf8')
        ) : {}
    );

    return to_return;
}

module.exports = (env, argv) => {
    var yamlcfg = read_config();

    const devMode = argv.mode !== 'production';
    const sourceMapsEnabled = devMode && true; // Set to false to disable sourcemaps

    // defaults for both modes
    var cfg = {
        mode: argv.mode,

        entry: {
            "app":        "./private/app/js/app",
            "vizdemo":        "./private/app/js/scrapper-1.2.0/scrapper-viz/demo"
        },

        output: {
            path:       path.resolve(__dirname, "public/assets"),
            publicPath: yamlcfg.site.urls.web_path + "assets/",
            // filename:   '[name].[hash].js'
        },

        devtool: "none",

        resolve: {
            extensions: ['.js', '.json', '.jsx', '.css', '.scss', '.ejs'],
            modules: [
                "node_modules",
                //"app",
                path.resolve(__dirname, "private/app/js/")
            ],
            alias: {
                "CBC": path.resolve(__dirname, "private/app/js/CBC/"),
                "CBCStyle": path.resolve(__dirname, "private/app/styles/"),

                // project shortcuts.. will this work ... ?
                "@project": path.resolve( __dirname, "private/app/"),
                "@project-images": path.resolve( __dirname, "private/app/static/images/"),
                "@project-fonts": path.resolve( __dirname, "private/app/static/fonts/"),
                "@project-data": path.resolve( __dirname, "private/app/static/data/"),
            }
        },

        module: {
            rules: [
	            {
	                test: /(\.jsx|\.js)$/,
	                exclude: /(node_modules|bower_components)/,
                    use: [{
                        loader: 'babel-loader',
                        options: {
                            presets: [
                                [
                                    "@babel/preset-env",
                                    // Delete the following object if you want to use your own polyfills
                                    {
                                        "targets": {
                                            "browsers": ["last 2 versions", "ie >= 11"],
                                        },
                                        "useBuiltIns": "usage",
                                        "corejs": 3
                                    }
                                ]
                            ]
                        }
                    },
                    // { loader: "eslint-loader", options: { configFile: path.resolve(__dirname, '.eslintrc') } }
                    ]
	            },
                {
                    test: /\.ejs$/,
                    use: [
                        { loader: 'ejs-compiled-loader' }
                    ]
                },
                {   test: /\.(woff|woff2|eot|ttf|otf)$/,
                    use: [
                        { loader: 'file-loader?name=f/[name].[hash].[ext]' }
                    ]
                },
                {   test: /\.(png|jpg|gif)$/,
                    use: [
                        { loader: 'file-loader?name=i/[name].[hash].[ext]' }
                    ]
                },
                {   test: /\.svg/,
                    use: {
                        loader: 'svg-url-loader', options: {}
                    }
                },
                {
                    test: /\.(sa|sc|c)ss$/,
                    use: argv.mode == 'production' ?
                    [
                        { loader: MiniCssExtractPlugin.loader, options: {
                            filename: "[name].[hash].css",
                            chunkFilename: "[id].[hash].css"
                        } },
                        //"style-loader", // creates style nodes from JS strings
                        "css-loader",
                        "sass-loader" // compiles Sass to CSS, using Node Sass by default
                    ]
                    :
                    [
                        { loader: MiniCssExtractPlugin.loader, options: {
                            filename: "[name].css",
                            chunkFilename: "[id].css",
                            // new as of 0.7.0:
                            hmr: true,
                            reloadAll: true
                        } },
                        // "style-loader", // creates style nodes from JS strings
                        "css-loader",
                        "sass-loader" // compiles Sass to CSS, using Node Sass by default
                    ]
                },
                {
                    test: /\.y(a)?ml$/,
                    use: 'js-yaml-loader',
                }
            ]
        },

        plugins: [
            new webpack.DefinePlugin({
                __SITE_CONFIG__:    JSON.stringify(yamlcfg.site),
                __WEBPATH__:        JSON.stringify(yamlcfg.site.urls.web_path),
                __MODE__:           JSON.stringify(yamlcfg.environment),
                __QAMODE__:         env && env === 'qabuild' ? true : false
            }),

            new webpack.SourceMapDevToolPlugin({
                'filename':     '[name].js.map',

                // i do not think this is working
                'exclude':       ['node_modules'],
            }),

            new MiniCssExtractPlugin({
                // Options similar to the same options in webpackOptions.output
                // both options are optional
                filename: devMode ? '[name].css' : '[name].[hash].css',
                chunkFilename: devMode ? '[id].css' : '[id].[hash].css',
            }),

            new HtmlWebpackPlugin({
                inject: false,
                filename:   path.resolve(__dirname, "private/app/templates/public/generated/webpack.twig"),
                template:
                    fs.existsSync( path.resolve(__dirname, "private/app/templates/public/generated/webpack.twig-template.ejs") ) ?
                        path.resolve(__dirname, "private/app/templates/public/generated/webpack.twig-template.ejs")
                        :
                        path.resolve(__dirname, "core/app/templates/generated/webpack.twig-template.ejs")
            }),

            // https://github.com/webpack-contrib/copy-webpack-plugin
            new CopyPlugin([
                // note: to paths are based from cfg.output.path
                { from: 'private/app/static/data', to: 'data/' },
                { from: 'private/app/static/images', to: 'images/' },
                { from: 'private/app/static/fonts', to: 'fonts/' },
                { from: 'private/app/static/video', to: 'video/' }
            ], { toType: 'dir', }),
        ]
    };


    if ( devMode ) {
        console.log("Using development options..");

        cfg.devtool = 'eval-source-map';

        // and append / overwrite cfg
        cfg.optimization = {
            splitChunks: {
                cacheGroups: {
                    commons: {
                        name: 'common',
                        chunks: 'initial',
                        minChunks: 2
                    }
                }
            }
        }

        cfg.devServer = {
            host:       '0.0.0.0',
            hot:        true, // Set this and inline to true for HMR/Live Reload (HMR Doesn't work very well)
            inline:     true,
            overlay:    true,


            public:     'newsinteractives-dev.cbc.ca:' + process.env.CBC_HMR_PORT,
            publicPath: yamlcfg.site.urls.web_path,
            port:       process.env.CBC_HMR_PORT,

            clientLogLevel:   'info',
            disableHostCheck: true,

            proxy: {
                '/': {
                    target: 'http://localhost/',
                    changeOrigin: true,
                }
            },

            // critical.
            writeToDisk: true
        };

        cfg.plugins.push( new webpack.HotModuleReplacementPlugin() );
    } else {
        console.log("Using production options to optimize assets..");

        // to help with caching
        cfg.output.filename = '[name].[hash].js';

        cfg.plugins.unshift( new CleanWebpackPlugin() );

        // ibid
        cfg.optimization = {
            splitChunks: {
                cacheGroups: {
                    commons: {
                        name: 'common',
                        chunks: 'initial',
                        minChunks: 2
                    }
                }
            },

            minimizer: [
                new TerserPlugin({
                    terserOptions: {
                        compress: {
                            drop_console: true
                        }
                    }
                }),

                new OptimizeCSSAssetsPlugin({})
            ]
        };

        if ( env === 'qabuild' || env === 'build' )
        {
            console.log('QABuild Activated (Environment=' + env + ')');

            cfg.plugins.push(
                new WebpackHookPlugin({
                    onBuildEnd: [
                        '/bin/rm -fr /cbc/cache/twig/*',
                        '/cbc/core/app/php/bin/cbc-web-prerender -p=dist -f=true',
                    ]
                })
            );

            cfg.plugins.push(
                new FileManagerPlugin({
                    onStart: {
                        delete: [
                            '/cbc/cache/twig/',
                        ]
                    },
                    onEnd: {
                        delete: [
                            'public/dist/'
                        ],
                        mkdir: [
                            'public/dist/'
                        ],
                        copy: [
                            // { source: 'public/index.html',  destination:    'public/dist/index.html' },
                            // { source: 'public/**/*.html',   destination:    'public/dist/' },
                            { source: 'public/assets',      destination:    'public/dist/assets/' },
                            { source: 'public/images',      destination:    'public/dist/images/' }
                        ]
                    }
                })
            );
        }
    }

    return cfg;
}
