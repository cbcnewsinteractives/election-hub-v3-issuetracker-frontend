docker run \
    --rm \
    -it  \
    -p 8080:8080 \
    -p 80:80     \
    -v `pwd`/private/:/cbc/private \
    -v `pwd`/public:/cbc/public \
    cbc/base-dev-simple \
    ;
