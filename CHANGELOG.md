
# Changelog

## 1.2.0

- Added section for pill styles
- Removed 300 weight Open Sans
- Added scaffolding for project-specific style folders to promote storing Sass files outside of scrapper directory
- Updated Readme with simplified instructions for creating a new project
- Added functionality so elements receive focus outline only when using keyboard
- Moved stats-top.js to the bottom of the page to increase page load speed
- Hide the share tools when page is loaded via CBC News app
- Added functionality to automatically click buttons when selected and user hits the space bar
- WIP: Added framework for D3-based visualizations
