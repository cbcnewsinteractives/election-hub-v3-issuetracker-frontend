// import h from './helpers';
// import $ from "../vendor/jquery-3.4.1.min";


// import h from 'helpers';
import $ from 'jquery';

export default {
	init: function() {


    var scrolling = false;
 
$( window ).scroll( function() {
  scrolling = true;
     document.querySelector('.ni-back-to-top').style.opacity = "1";
});
 
setInterval( function() {
  if ( scrolling ) {
    scrolling = false;
   
  }
}, 500 );
 


     // Execute on load
    
      var clickedEl, analysisHeight, relatedHeight, buttonText, arrowSVG, fader, fullHeight, buttonHeight, bylineHeight, issueName;

   // First check width so we can add up the contents differently depending on whether they are stacked or adjacent

  function checkWidth() {
        var windowSize = $(window).width();

        if (windowSize < 980) {
            // console.log("screen width is less than 980");

     $(document).on('mousedown touchend keypress','.buttonContainer',function() {

          // console.log(windowSize);

              clickedEl = $(this).closest(".ni-grid-parent.analysis");
              issueName = $(clickedEl).attr("id");
              analysisHeight = $(clickedEl).find("p.analysis").outerHeight();
              relatedHeight = $(clickedEl).find(".issue-related").outerHeight();
              buttonText = $(clickedEl).find(".read-more").text();


        setTimeout(() => {
          if (window.Waypoint) {
            window.Waypoint.refreshAll();
          }
        }, 200);



     if (buttonText == "Show analysis") {

      window.parent.CBC.APP.SC.EventTracker.trackEvent("Opened",{feature:{name:"issue.analysis",position:issueName}});


      // fader.fadeOut();
        //Stuff to do when btn is in the read more state
        $(clickedEl).find(".buttonContainer").attr("aria-expanded","true");
        $(clickedEl).find(".read-more").text("Hide analysis");
        $(clickedEl).find(".arrowIcon").addClass("rotated");
        $(clickedEl).find(".fadeout").fadeTo( "slow", 0 );
        $(clickedEl).find(".fadeout").addClass("background");
        $(clickedEl).find(".related-container ul li a").removeAttr("tabindex");
        $(clickedEl).find(".related-container ul li a").removeAttr("aria-hidden");

        // measure how tall inside should be by adding together heights of all inside paragraphs

        // fullHeight = (relatedHeight + 190) + analysisHeight;

            if (isNaN(relatedHeight)) {
                    fullHeight = analysisHeight + 130;
                  } else {
                    fullHeight = (relatedHeight + 150) + analysisHeight;
                  }


                    $(clickedEl).animate({height:fullHeight},300);

          
    } else {


      //Stuff to do when btn is in the read less state
        $(clickedEl).find(".read-more").text("Show analysis");
        // fader.fadeIn();
    $(clickedEl).find(".arrowIcon").removeClass("rotated");
    $(clickedEl).find(".buttonContainer").attr("aria-expanded","false");
    $(clickedEl).find(".fadeout").fadeTo( "slow", 1 );
    $(clickedEl).find(".fadeout").removeClass("background");
    $(clickedEl).find(".related-container ul li a").attr('tabindex', '-1');
    $(clickedEl).find(".related-container ul li a").attr('aria-hidden', 'true');
    // $(clickedEl).find(".related-container").removeClass("unHide");

    
      clickedEl.animate({
    "height": 140
    });
    }
          event.stopImmediatePropagation();
  return false;     
    });


        }
        else {
            // console.log("screen width is greater than or equal to 980");

            $(document).on('mousedown touchend keypress','.buttonContainer',function() {

 
                    clickedEl = $(this).closest(".ni-grid-parent.analysis");
                    issueName = $(clickedEl).attr("id");
                    analysisHeight = $(clickedEl).find("p.analysis").outerHeight();
                    relatedHeight = $(clickedEl).find(".issue-related").outerHeight();
                    buttonText = $(clickedEl).find(".read-more").text();

                  // Refresh timeouts after animation has completed
                  setTimeout(() => {
                    if (window.Waypoint) {
                      window.Waypoint.refreshAll();
                    }
                  }, 200);



                 if (buttonText == "Show analysis") {

                   window.parent.CBC.APP.SC.EventTracker.trackEvent("Opened",{feature:{name:"issue.analysis",position:issueName}});

                  // fader.fadeOut();
                    //Stuff to do when btn is in the read more state
                    $(clickedEl).find(".read-more").text("Hide analysis");
                    $(clickedEl).find(".buttonContainer").attr("aria-expanded","true");
                    $(clickedEl).find(".arrowIcon").addClass("rotated");
                    $(clickedEl).find(".fadeout").fadeTo( "slow", 0 );
                    $(clickedEl).find(".fadeout").addClass("background");
                    $(clickedEl).find(".issue-related ul li a").removeAttr("tabindex");
                    $(clickedEl).find(".issue-related ul li a").removeAttr("aria-hidden");
                 
                    // measure how tall inside should be by adding together heights of all inside paragraphs

                    if (isNaN(relatedHeight)) {
                    fullHeight = analysisHeight;
                  } else {
                    fullHeight = (relatedHeight + 40) + analysisHeight;
                  }


                    $(clickedEl).animate({height:fullHeight},300);

                      
                } else {
                  //Stuff to do when btn is in the read less state
                    $(clickedEl).find(".read-more").text("Show analysis");
                    // fader.fadeIn();
                $(clickedEl).find(".arrowIcon").removeClass("rotated");
                $(clickedEl).find(".buttonContainer").attr("aria-expanded","false");
                $(clickedEl).find(".fadeout").fadeTo( "slow", 1 );
                $(clickedEl).find(".fadeout").removeClass("background");
                $(clickedEl).find(".issue-related ul li a").attr('tabindex', '-1');
                $(clickedEl).find(".issue-related ul li a").attr('aria-hidden', 'true');

                // $(clickedEl).find(".related-container").removeClass("unHide");
                setTimeout(() => {
                  if (window.Waypoint) {
                    window.Waypoint.refreshAll();
                  }
                }, 200);

                  clickedEl.animate({
                "height": 120
                });
                }
                      event.stopImmediatePropagation();
              return false;
                });


        }
    }

    // Execute on load
    checkWidth();
    // Bind event listener
    // $(window).resize(checkWidth);



  }
}

$('.insideLink').click(function(event){
    event.stopImmediatePropagation();
});
