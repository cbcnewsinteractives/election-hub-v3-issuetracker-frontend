// Activate any link buttons when focused and the user hits the space bar
export default {
  init: () => {
    document.addEventListener('keydown', (e) => {
      if (e.keyCode === 32 && (document.activeElement.classList.contains('ni-button') || document.activeElement.getAttribute('role') === 'button')) {
        document.activeElement.click();
        e.preventDefault();
      }
    });
  },
};
