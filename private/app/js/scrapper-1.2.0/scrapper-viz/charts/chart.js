import * as d3 from 'd3';
import deepmerge from 'deepmerge';

// Merge pattern used for deepmerge
const overwriteMerge = (destinationArray, sourceArray) => sourceArray;

class Chart {
  constructor(element, config, schema) {
    // Ensure that chart container exists on the page
    if (!element) {
      console.error(`${this.constructor.name} can not find container element on the page.`);
      return;
    }
    this.element = element;
    this.element.classList.add('ni-chart');

    // define width, height and margin
    this.width = this.element.offsetWidth;
    this.height = this.width / 2;

    this.svg = d3.select(this.element).append('svg');
    this.svg.attr('width', this.width);
    this.svg.attr('height', this.height);

    // Validate the config against the schema
    this.schema = schema;
    this.validateConfig(config);

    // Merge default margins with config-set margins
    const defaultMargin = {
      top: 20, right: 50, bottom: 45, left: 50,
    };
    this.margin = deepmerge(defaultMargin, this.config.margin, { arrayMerge: overwriteMerge });

    // Keep everything inside a <g> element
    this.chart = this.svg.append('g')
      .attr('transform', `translate(${this.margin.left},${this.margin.top})`);

    // Respond to window resizes
    this.handleResize();

    // Create a generic tooltip
    this.tooltip = d3.select(this.element).append('div')
      .attr('class', 'ni-chart__tooltip')
      .style('opacity', 0);

    // Declare some variables we want to use with all of our charts
    this.transitionDuration = 600;
  }

  // Check that config is valid against schema and set config variable
  validateConfig(config) {
    try {
      this.config = this.schema.validateSync(config, { abortEarly: false });
    } catch (err) {
      console.log(err);
      console.error(`${this.constructor.name}'s configuration is incorrect: ${err.errors.join('; ')}`);
    }
  }

  // Allow updates to the data or other config settings
  updateConfig(config) {
    const newConfig = deepmerge(this.config, config, { arrayMerge: overwriteMerge });
    this.validateConfig(newConfig);
    this.draw();
  }

  // Handle a re-draw on window resize. Can be overridden by child classes if something special needs to be done.
  handleResize() {
    window.addEventListener('resize', () => {
      this.width = this.element.offsetWidth;
      this.height = this.width / 2;

      this.svg.attr('width', this.width);
      this.svg.attr('height', this.height);

      this.setupScales();

      this.draw();
    });
  }

  // Child classes should implement these, so throw a warning if they haven't
  setupScales() {
    console.error(`${this.constructor.name} must implement the setupScales() method`);
  }

  draw() {
    console.error(`${this.constructor.name} must implement the draw() method`);
  }
}

export default Chart;
