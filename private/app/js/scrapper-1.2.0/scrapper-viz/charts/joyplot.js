import * as d3 from 'd3';
import * as yup from 'yup';

import Chart from './chart';

// Define the schema for this chart's options so we can validate individual chart configs
const schema = yup.object().shape({
  data: yup.array().of(yup.object({
    label: yup.string().default(''),
    color: yup.string(),
    values: yup.array().of(yup.number()).required(),
  })).required(),
  xseries: yup.array().required(),
  xaxis: yup.object({
    label: yup.string().default(''),
  }).default({}),
  yaxis: yup.object({
    label: yup.string().default(''),
  }).default({}),
  margin: yup.object({
    top: yup.number(),
    right: yup.number(),
    bottom: yup.number(),
    left: yup.number(),
  }),
  tooltip: yup.object({
    formatter: yup.object(),
  }).default({}),
});

class Joyplot extends Chart {
  constructor(element, config) {
    // Invoke parent class' constructor. You shouldn't need to change this.
    super(element, config, schema);

    this.overlap = 0.8; // Percent each area chart should overlap
    this.rowHeight = 25; // Height for each area chart
    this.height = this.config.data.length * this.rowHeight + this.margin.top + this.margin.bottom;
    this.svg.attr('height', this.height);

    // Add custom class to container
    this.element.classList.add('ni-joyplot');

    this.setupScales();

    // Add x, y, and category axes
    this.chart.append('g').attr('class', 'ni-chart__axis ni-chart__axis--x');
    this.chart.append('g').attr('class', 'ni-chart__axis ni-chart__axis--y');
    this.chart.append('g').attr('class', 'ni-chart__axis ni-chart__axis--category');

    // Add x axis label
    // this.chart.append('text').attr('class', 'ni-chart__axis-label ni-chart__axis-label--x').style('text-anchor', 'end');

    // Do an initial render of the chart
    this.draw();
  }

  // Set the scales (how our data translates to screen positions) and associate with axes
  // We automatically detect scale type based on the data (this could use improvement)
  setupScales() {
    const xScaleType = this.config.xseries[0] instanceof Date ? d3.scaleTime : d3.scaleLinear;
    this.xScale = xScaleType().range([0, this.width - this.margin.right]);
    this.xAxis = d3.axisBottom().scale(this.xScale).ticks(this.width / 80);

    this.yScale = d3.scaleLinear().range([this.rowHeight * (1 + this.overlap), 0]);

    this.category = (d) => d.label;
    this.categoryScale = d3.scaleBand().range([0, (this.height - this.margin.top - this.margin.bottom)]).paddingInner(1);
    this.categoryAxis = d3.axisLeft().scale(this.categoryScale);
  }

  draw() {
    // calculate max and min values for data to use on axes
    // TODO: Allow this to be set in config
    const xExtent = d3.extent(this.config.xseries);
    const yExtent = d3.extent(this.config.data.map((item) => item.values).flat());

    const categories = this.config.data.map((item) => item.label);

    //
    // Set the X axis:
    //
    this.xScale.domain(xExtent);
    this.chart.selectAll('.ni-chart__axis--x').transition()
      .duration(this.transitionDuration)
      .call(this.xAxis.ticks(this.width / 80));

    //
    // Set the Y axis
    //
    this.yScale.domain(yExtent);

    //
    // Set the categories
    //
    this.categoryScale.domain(categories);
    this.chart.selectAll('.ni-chart__axis--category')
      .transition()
      .duration(this.transitionDuration)
      .call(this.categoryAxis);

    // Update x axis position and labels
    this.chart.select('.ni-chart__axis--x')
      .attr('transform', `translate(0, ${this.height + this.margin.top - this.margin.bottom})`);

    //
    // Areas: data relationship and enter/update/exit behavior
    //
    const area = d3.area()
      .curve(d3.curveMonotoneX)
      .defined((d) => !Number.isNaN(d))
      .x((d, i) => this.xScale(this.config.xseries[i]))
      .y0(this.yScale(0))
      .y1((d) => this.yScale(d));
    const areas = this.chart.selectAll('path.ni-joyplot__path').data(this.config.data);

    areas.enter()
      .append('path')
      .attr('id', (d, i) => `ni-joyplot__path-${i}`)
      .attr('class', 'ni-joyplot__path')
      .merge(areas)
      .transition()
      .duration(this.transitionDuration)
      .attr('d', (d) => area(d.values))
      .attr('transform', (d) => `translate(0,${(this.categoryScale(d.label) - (this.rowHeight * (1 + this.overlap))) + 1})`);

    //  TODO: Hover states
  }
}

export default Joyplot;
