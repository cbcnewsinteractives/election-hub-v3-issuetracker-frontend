import * as d3 from 'd3';
import * as yup from 'yup';

import Chart from './chart';


// Define the schema for this chart's options so we can validate individual chart configs
const schema = yup.object().shape({
  data: yup.array().of(yup.object({
    label: yup.string().default(''),
    color: yup.string(),
    values: yup.array().of(yup.number()).required(),
  })).required(),
  bins: yup.number().required(),
  xaxis: yup.object({
    label: yup.string().default(''),
  }).default({}),
  yaxis: yup.object({
    label: yup.string().default(''),
  }).default({}),
  margin: yup.object({
    top: yup.number(),
    right: yup.number(),
    bottom: yup.number(),
    left: yup.number(),
  }),
  tooltip: yup.object({
    formatter: yup.object(),
  }).default({}),
});

class Histogram extends Chart {
  constructor(element, config) {
    // Invoke parent class' constructor. You shouldn't need to change this.
    super(element, config, schema);

    // Add custom class to container
    this.element.classList.add('ni-histogram');

    this.setupScales();

    // Add x and y axes
    this.chart.append('g').attr('class', 'ni-chart__axis ni-chart__axis--x');
    this.chart.append('g').attr('class', 'ni-chart__axis ni-chart__axis--y');

    // Add axis labels
    this.chart.append('text').attr('class', 'ni-chart__axis-label ni-chart__axis-label--x').style('text-anchor', 'end');
    this.chart.append('text').attr('class', 'ni-chart__axis-label ni-chart__axis-label--y').attr('transform', 'rotate(-90)').attr('dy', '1em');

    // Do an initial render of the chart
    this.draw();
  }

  // Set the scales (how our data translates to screen positions) and associate with axes
  // We automatically detect scale type based on the data
  // TODO: Allow for different scale types (e.g. category, log) -- Based on config value?
  setupScales() {
    this.xScale = d3.scaleLinear().range([0, this.width - this.margin.right]);
    this.xAxis = d3.axisBottom().scale(this.xScale);

    this.yScale = d3.scaleLinear().range([this.height - (this.margin.top + this.margin.bottom), 0]);
    this.yAxis = d3.axisLeft().scale(this.yScale);
  }

  draw() {
    //
    // Set the X axis
    //
    const allValues = this.config.data.map((item) => item.values).flat();
    const xMax = d3.max(allValues);
    this.xScale.domain([0, xMax]);
    this.chart.selectAll('.ni-chart__axis--x').transition()
      .duration(this.transitionDuration)
      .call(this.xAxis);

    //
    // Set the Y axis
    //
    this.yScale.domain([0, d3.max(this.getBinMaximums())]);
    this.chart.selectAll('.ni-chart__axis--y')
      .transition()
      .duration(this.transitionDuration)
      .call(this.yAxis);

    // Update x axis position and labels
    this.chart.select('.ni-chart__axis--x')
      .attr('transform', `translate(0, ${this.height - (this.margin.top + this.margin.bottom)})`);

    this.chart.select('.ni-chart__axis-label--x')
      .attr('transform', `translate(${this.width / 2}, ${this.height - 24})`)
      .text(this.config.xaxis.label);
    this.chart.select('.ni-chart__axis-label--y')
      .attr('y', 0 - this.margin.left)
      .attr('x', 0 - (this.height / 2))
      .text(this.config.yaxis.label);

    //
    // Draw histogram for each data point
    //
    this.config.data.forEach((data, i) => {
      this.drawHistogram(data, i);
    });

    // TODO: Hover states
  }

  getBinMaximums() {
    const binMax = [];
    this.config.data.forEach((data) => {
      const histogram = d3.histogram().domain(this.xScale.domain()).thresholds(this.xScale.ticks(this.config.bins));
      const bins = histogram(data.values);

      binMax.push(d3.max(bins, (d) => d.length));
    });

    return binMax;
  }

  drawHistogram(data, index) {
    //
    // Set histogram data
    //
    const histogram = d3.histogram().domain(this.xScale.domain()).thresholds(this.xScale.ticks(this.config.bins));
    const bins = histogram(data.values);

    //
    // Draw histogram
    //
    const bars = this.chart.selectAll(`.ni-histogram__bar--${index}`).data(bins);

    bars.enter()
      .append('rect')
      .attr('id', (d, i) => `ni-histogram__bar-${index}-${i}`)
      .attr('class', `ni-histogram__bar ni-histogram__bar--${index}`)
      .merge(bars)
      .transition()
      .duration(this.transitionDuration)
      .attr('x', 1)
      .attr('transform', (d) => `translate(${this.xScale(d.x0)},${this.yScale(d.length)})`)
      .attr('width', (d) => this.xScale(d.x1) - this.xScale(d.x0) - 1)
      .attr('height', (d) => this.height - (this.margin.top + this.margin.bottom) - this.yScale(d.length))
      .attr('fill', data.color || '#000000');

    bars.exit().remove();
  }
}

export default Histogram;
