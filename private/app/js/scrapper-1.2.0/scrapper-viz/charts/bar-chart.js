import * as d3 from 'd3';
import * as yup from 'yup';

import Chart from './chart';


// Define the schema for this chart's options so we can validate individual chart configs
const schema = yup.object().shape({
  data: yup.array().of(yup.object({
    label: yup.string().default(''),
    color: yup.string(),
    values: yup.array().of(yup.number()).required(),
  })).required(),
  xseries: yup.array().required(),
  xaxis: yup.object({
    padding: yup.number().default(0.3),
    label: yup.string().default(''),
  }).default({}),
  yaxis: yup.object({
    label: yup.string().default(''),
  }).default({}),
  margin: yup.object({
    top: yup.number(),
    right: yup.number(),
    bottom: yup.number(),
    left: yup.number(),
  }),
  tooltip: yup.object({
    formatter: yup.object(),
  }).default({}),
});

class BarChart extends Chart {
  constructor(element, config) {
    // Invoke parent class' constructor. You shouldn't need to change this.
    super(element, config, schema);

    // Add custom class to container
    this.element.classList.add('ni-bar-chart');

    this.setupScales();

    // Add x and y axes
    this.chart.append('g').attr('class', 'ni-chart__axis ni-chart__axis--x');
    this.chart.append('g').attr('class', 'ni-chart__axis ni-chart__axis--y');

    // Add axis labels
    this.chart.append('text').attr('class', 'ni-chart__axis-label ni-chart__axis-label--x').style('text-anchor', 'end');
    this.chart.append('text').attr('class', 'ni-chart__axis-label ni-chart__axis-label--y').attr('transform', 'rotate(-90)').attr('dy', '1em');

    // Do an initial render of the chart
    this.draw();
  }

  // Set the scales (how our data translates to screen positions) and associate with axes
  // We automatically detect scale type based on the data
  // TODO: Allow for different scale types (e.g. category, log) -- Based on config value?
  setupScales() {
    this.xScale = d3.scaleBand().range([0, this.width - this.margin.right]).padding(this.config.xaxis.padding);
    this.xAxis = d3.axisBottom().scale(this.xScale);

    const yScaleType = this.config.data[0].values[0][1] instanceof Date ? d3.scaleTime : d3.scaleLinear;
    this.yScale = yScaleType().range([this.height - (this.margin.top + this.margin.bottom), 0]);
    this.yAxis = d3.axisLeft().scale(this.yScale);
  }

  draw() {
    // calculate max value of summed data values to determine y-axis height
    // TODO: Allow this to be overridden in config
    const summedValues = [];
    this.config.data.forEach((item) => {
      for (let i = 0; i < this.config.xseries.length; i++) {
        summedValues[i] = (summedValues[i] || 0) + item.values[i];
      }
    });
    const yMax = d3.max(summedValues);

    //
    // Set the X axis:
    //
    this.xScale.domain(this.config.xseries);
    this.chart.selectAll('.ni-chart__axis--x').transition()
      .duration(this.transitionDuration)
      .call(this.xAxis);

    //
    // Set the Y axis
    //
    this.yScale.domain([0, yMax]);
    this.chart.selectAll('.ni-chart__axis--y')
      .transition()
      .duration(this.transitionDuration)
      .call(this.yAxis);

    const colorScale = d3.scaleOrdinal()
      .domain(this.config.xseries)
      .range(this.config.data.map((d) => d.color));

    // Update x axis position and labels
    this.chart.select('.ni-chart__axis--x')
      .attr('transform', `translate(0, ${this.height - (this.margin.top + this.margin.bottom)})`);

    this.chart.select('.ni-chart__axis-label--x')
      .attr('transform', `translate(${this.width / 2}, ${this.height - 24})`)
      .text(this.config.xaxis.label);
    this.chart.select('.ni-chart__axis-label--y')
      .attr('y', 0 - this.margin.left)
      .attr('x', 0 - (this.height / 2))
      .text(this.config.yaxis.label);

    //
    // Hover states for bars
    //
    // Not using arrow functions or class methods because d3 does trixy stuff binding itself as `this` context in event handlers
    const self = this;
    const mouseover = function mouseover() {
      self.tooltip.style('opacity', 1);
    };
    const mousemove = function mousemove(d) {
      const groupLabel = d.data.label;
      const subgroupLabel = d3.select(this.parentNode).datum().key;
      const subgroupValue = d.data[subgroupLabel];

      // Set tooltip content using function passed from config
      const tooltipContent = self.config.tooltip.formatter
        ? self.config.tooltip.formatter(groupLabel, subgroupLabel, subgroupValue)
        : `${groupLabel}: ${subgroupLabel} (${subgroupValue})`;

      self.tooltip
        .html(tooltipContent)
        .style('left', `${d3.mouse(this)[0] + 70}px`)
        .style('top', `${d3.mouse(this)[1]}px`);
    };
    const mouseleave = function mouseleave() {
      // Reset styles
      self.tooltip.style('opacity', 0);
    };

    //
    // Bars: data relationship and enter/update/exit behavior
    //
    // Transform data to array of objects with named accessors so d3.stack is happy
    const transformedData = [];
    this.config.xseries.forEach((column, i) => {
      const item = { label: column };
      this.config.data.forEach((row) => {
        item[row.label] = row.values[i];
      });
      transformedData.push(item);
    });
    const stackGenerator = d3.stack().keys(this.config.data.map((d) => d.label));
    const stackedData = stackGenerator(transformedData);

    // I'll be honest this doesn't make a lot of sense to me but transitioning nested objects is a beeotch
    const barGroups = this.chart.selectAll('.ni-bar-chart__bar-group').data(stackedData);
    const bars = barGroups.enter()
      .append('g')
      .merge(barGroups)
      .attr('class', 'ni-bar-chart__bar-group')
      .attr('fill', (d) => colorScale(d.key) || '#000000')
      .selectAll('rect')
      .data((d) => d);

    const barsEnter = bars.enter().append('rect').attr('class', 'ni-bar-chart__bar-segment');

    bars.exit().transition().duration(this.transitionDuration);

    barsEnter.merge(bars)
      .on('mouseover', mouseover)
      .on('mousemove', mousemove)
      .on('mouseleave', mouseleave)
      .transition()
      .duration(this.transitionDuration)
      .attr('x', (d) => this.xScale(d.data.label))
      .attr('y', (d) => this.yScale(d[1]))
      .attr('height', (d) => this.yScale(d[0]) - this.yScale(d[1]))
      .attr('width', this.xScale.bandwidth());

    barGroups.exit().remove();
  }
}

export default BarChart;
