import * as d3 from 'd3';
import * as yup from 'yup';

import Chart from './chart';

// Define the schema for this chart's options so we can validate individual chart configs
const schema = yup.object().shape({
  data: yup.array().of(yup.object({
    label: yup.string().default(''),
    color: yup.string(),
    dasharray: yup.string(),
    values: yup.array().of(yup.array().min(2).max(2)).required(),
  })).required(),
  xaxis: yup.object({
    label: yup.string().default(''),
  }).default({}),
  yaxis: yup.object({
    label: yup.string().default(''),
  }).default({}),
  margin: yup.object({
    top: yup.number(),
    right: yup.number(),
    bottom: yup.number(),
    left: yup.number(),
  }),
  tooltip: yup.object({
    formatter: yup.object(),
  }).default({}),
});

class LineChart extends Chart {
  constructor(element, config) {
    // Invoke parent class' constructor. You shouldn't need to change this.
    super(element, config, schema);

    // Increase our right margin to give space to line labels
    this.margin.right = this.config.margin.right ? this.config.margin.right : 120;

    // Add custom class to container
    this.element.classList.add('ni-line-chart');

    this.setupScales();

    // Add x and y axes
    this.chart.append('g').attr('class', 'ni-chart__axis ni-chart__axis--x');
    this.chart.append('g').attr('class', 'ni-chart__axis ni-chart__axis--y');

    // Add axis labels
    this.chart.append('text').attr('class', 'ni-chart__axis-label ni-chart__axis-label--x').style('text-anchor', 'end');
    this.chart.append('text').attr('class', 'ni-chart__axis-label ni-chart__axis-label--y').attr('transform', 'rotate(-90)').attr('dy', '1em');

    // Do an initial render of the chart
    this.draw();
  }

  // Set the scales (how our data translates to screen positions) and associate with axes
  // We automatically detect scale type based on the data
  // TODO: Allow for different scale types (e.g. category, log) -- Based on config value?
  setupScales() {
    const xScaleType = this.config.data[0].values[0][0] instanceof Date ? d3.scaleTime : d3.scaleLinear;
    this.xScale = xScaleType().range([0, this.width - this.margin.right]);
    this.xAxis = d3.axisBottom().scale(this.xScale);

    const yScaleType = this.config.data[0].values[0][1] instanceof Date ? d3.scaleTime : d3.scaleLinear;
    this.yScale = yScaleType().range([this.height - (this.margin.top + this.margin.bottom), 0]);
    this.yAxis = d3.axisLeft().scale(this.yScale);
  }

  draw() {
    // calculate max and min values for data to use on axes
    // TODO: Allow this to be set in config
    const xExtent = d3.extent(this.config.data.map((item) => item.values).flat(), (d) => d[0]);
    const yExtent = d3.extent(this.config.data.map((item) => item.values).flat(), (d) => d[1]);

    //
    // Set the X axis:
    //
    this.xScale.domain(xExtent);
    this.chart.selectAll('.ni-chart__axis--x').transition()
      .duration(this.transitionDuration)
      .call(this.xAxis.ticks(this.width / 100));

    //
    // Set the Y axis
    //
    this.yScale.domain(yExtent);
    this.chart.selectAll('.ni-chart__axis--y')
      .transition()
      .duration(this.transitionDuration)
      .call(this.yAxis);

    // Update x axis position and labels
    this.chart.select('.ni-chart__axis--x')
      .attr('transform', `translate(0, ${this.height - (this.margin.top + this.margin.bottom)})`);

    this.chart.select('.ni-chart__axis-label--x')
      .attr('transform', `translate(${this.width / 2}, ${this.height - 24})`)
      .text(this.config.xaxis.label);
    this.chart.select('.ni-chart__axis-label--y')
      .attr('y', 0 - this.margin.left)
      .attr('x', 0 - (this.height / 2))
      .text(this.config.yaxis.label);

    //
    // Lines: data relationship and enter/update/exit behavior
    //
    const line = d3.line().x((d) => this.xScale(d[0])).y((d) => this.yScale(d[1])).curve(d3.curveMonotoneX);
    const lines = this.chart.selectAll('.ni-line-chart__line').data(this.config.data);

    lines.enter()
      .append('path')
      .attr('id', (d, i) => `ni-line-chart__line-${i}`)
      .attr('class', 'ni-line-chart__line')
      .merge(lines)
      .transition()
      .duration(this.transitionDuration)
      .attr('d', (d) => line(d.values))
      .attr('fill', 'none')
      .attr('stroke', (d) => d.color || '#000000')
      .attr('stroke-dasharray', (d) => d.dasharray || '1');

    lines.exit().remove();

    //
    // Hover lines: Same as lines above but fatter and invisible (because 1px lines are shitty hover targets)
    //
    const self = this;
    let activeLine = false;

    // Not using arrow functions or class methods because d3 does trixy stuff binding itself as `this` context in event handlers
    const mouseover = function mouseover() {
      self.tooltip.style('opacity', 1);

      // Find the related visible line to this (invisible) hover line
      const id = d3.select(this).attr('id').split('-').pop();
      activeLine = d3.select(`#ni-line-chart__line-${id}`);
      activeLine.style('stroke-width', 4);

      // Dim other strokes
      d3.selectAll('path.ni-line-chart__line').transition().style('opacity', function () {
        return (d3.select(this).attr('id') === activeLine.attr('id')) ? 1 : 0.3;
      });
    };
    const mousemove = function mousemove(d) {
      // Set tooltip content
      const xVal = self.xScale.invert(d3.mouse(this)[0]);
      const yVal = self.yScale.invert(d3.mouse(this)[1]);
      const tooltipContent = self.config.tooltip.formatter ? self.config.tooltip.formatter(xVal, yVal, d) : `${xVal}: ${yVal}`;

      self.tooltip
        .html(tooltipContent)
        .style('left', `${d3.mouse(this)[0] + 70}px`)
        .style('top', `${d3.mouse(this)[1]}px`);
    };
    const mouseleave = function mouseleave() {
      // Reset styles
      self.tooltip.style('opacity', 0);
      activeLine.style('stroke-width', 2);
      d3.selectAll('path.ni-line-chart__line').transition().style('opacity', 1);
    };
    const hoverLines = this.chart.selectAll('.ni-line-chart__hover-line').data(this.config.data);

    hoverLines.enter()
      .append('path')
      .attr('class', 'ni-line-chart__hover-line')
      .attr('id', (d, i) => `ni-line-chart__hover-line-${i}`)
      .on('mouseover', mouseover)
      .on('mousemove', mousemove)
      .on('mouseleave', mouseleave)
      .merge(hoverLines)
      .attr('d', (d) => line(d.values))
      .attr('fill', 'none')
      .attr('stroke', '#000000')
      .style('opacity', 0)
      .attr('stroke-width', 8);

    hoverLines.exit().remove();

    //
    // Line legends
    //
    const labels = this.chart.selectAll('.ni-line-chart__line-label')
      .data(this.config.data);

    labels.enter()
      .append('text')
      .attr('class', 'ni-line-chart__line-label')
      .merge(labels)
      .transition()
      .attr('transform',
        (d) => `translate(${this.xScale(d.values[d.values.length - 1][0])},${this.yScale(d.values[d.values.length - 1][1])})`)
      .attr('x', 12) // shift the text a bit more right
      .text((d) => d.label)
      .style('fill', (d) => d.color || '#000000');

    labels.exit().remove();

    // Works but doesn't update correctly
    // const dots = this.chart.selectAll('.ni-line-chart__dots')
    //   .data(this.config.data);

    // dots.enter()
    //   .append('g')
    //     .attr('class', 'ni-line-chart__dots')
    //     // .merge(dots)
    //     .style('fill', d => d.color || '#000000')
    //     .selectAll('.ni-line-chart__dot')
    //     .data(d => d.values)
    //     .enter()
    //     // .join('circle')
    //     .append('circle')
    //       // .transition()
    //       // .duration(this.transitionDuration)
    //       .attr('class', 'ni-line-chart__dot')
    //       .attr('cx', d => this.xScale(d[0]) )
    //       .attr('cy', d => this.yScale(d[1]) )
    //       .attr('r', 5)
    //       .attr('stroke', 'white')
    //       .attr('stroke-width', 1);
  }
}

export default LineChart;
