// Demo configurations for ScrapperViz
require('../../app.js'); // Bring in the main app.js, which includes all the styles and base components

// Import our charting tools (for real projects only import the modules you need)
import {
  BarChart, Histogram, Joyplot, LineChart,
} from './index';


// Helper to generate an array of dates
const startDate = new Date();
const futureDate = new Date();
const getDates = (startDate, stopDate) => {
  const dates = [];
  const currentDate = startDate;
  while (currentDate <= stopDate) {
    dates.push(new Date(currentDate));
    currentDate.setDate(currentDate.getDate() + 1);
  }
  return dates;
};


const init = () => {
  // Bail if we're not on the demo page
  if (!document.querySelector('section.scrapper-viz')) {
    return;
  }


  //
  // Line Chart
  //
  const lineChartConfig = {
    data: [
      {
        label: 'Ice cream',
        color: '#57cef2',
        dasharray: '2,6',
        values: [
          [new Date(2016, 0, 1), 5], // x, y values
          [new Date(2016, 1, 1), 70],
          [new Date(2016, 2, 1), 30],
          [new Date(2016, 3, 1), 10],
          [new Date(2016, 4, 1), 40],
        ],
      },
      {
        label: 'Pie',
        color: '#eb34a1',
        dasharray: '1,2',
        values: [
          [new Date(2016, 0, 1), 30], // x, y values
          [new Date(2016, 1, 1), 20],
          [new Date(2016, 2, 1), 80],
          [new Date(2016, 3, 1), 10],
          [new Date(2016, 4, 1), 30],
        ],
      },
    ],
    xaxis: {
      label: 'I am the X axis',
    },
    yaxis: {
      label: 'I am the Y axis',
    },
    margin: {
      right: 120,
      bottom: 55,
    },
    tooltip: {
      formatter: (x, y, line) => {
        const formattedDate = new Date(x).toLocaleDateString();
        const formattedAmount = Math.floor(y);
        return `On ${formattedDate}, you ate ${formattedAmount} ${line.label}`;
      },
    },
  };
  const lineChart = new LineChart(document.getElementById('line-chart'), lineChartConfig);

  // Set up 'data update' button for this chart
  document.getElementById('line-chart-update').addEventListener('click', (e) => {
    e.preventDefault();
    lineChart.updateConfig({
      data: [
        {
          label: 'Ice cream',
          color: '#57cef2',
          values: [
            [new Date(2016, 0, 1), Math.floor(Math.random() * 100) + 1],
            [new Date(2016, 1, 1), Math.floor(Math.random() * 100) + 1],
            [new Date(2016, 2, 1), Math.floor(Math.random() * 100) + 1],
            [new Date(2016, 3, 1), Math.floor(Math.random() * 100) + 1],
            [new Date(2016, 4, 1), Math.floor(Math.random() * 100) + 1],
          ],
        },
      ],
    });
  });


  //
  // Bar Chart
  //
  futureDate.setDate(startDate.getDate() + 5);
  const barChartConfig = {
    data: [
      {
        label: 'Ice Cream',
        color: '#eb34a1',
        values: [2, 14, 7, 6, 0, 2],
      },
      {
        label: 'Cheesecake',
        color: '#36d7ac',
        values: [4, 19, 5, 10, 19, 8],
      },
      {
        label: 'Pie',
        color: '#57cef2',
        values: [14, 12, 13, 18, 4, 13],
      },
    ],
    xseries: getDates(startDate, futureDate).map((d) => d.toLocaleDateString('default', { month: 'short', day: 'numeric' })), // To save space, using date generator here
    xaxis: {
      label: 'I am the X axis',
      padding: 0.33,
    },
    yaxis: {
      label: 'I am the Y axis',
    },
    margin: {
      bottom: 55,
    },
    tooltip: {
      formatter: (groupLabel, subgroupLabel, subgroupValue) => `On ${groupLabel} you ate ${subgroupValue} ${subgroupLabel}`,
    },
  };
  const barChart = new BarChart(document.getElementById('bar-chart'), barChartConfig);

  // Set up 'data update' button for this chart
  document.getElementById('bar-chart-update').addEventListener('click', (e) => {
    e.preventDefault();
    barChart.updateConfig({
      data: [
        {
          label: 'Ice Cream',
          color: '#eb34a1',
          values: Array.from({ length: 6 }, () => Math.floor(Math.random() * 20)),
        },
        {
          label: 'Pie',
          color: '#57cef2',
          values: Array.from({ length: 6 }, () => Math.floor(Math.random() * 20)),
        },
      ],
    });
  });


  //
  // Histogram
  //
  const histogramConfig = {
    data: [
      {
        label: 'Ice cream',
        color: '#eb34a1',
        // Generate a bunch of random numbers with value < 40:
        values: Array.from({ length: 500 }, () => Math.floor(Math.random() * 40)),
      },
    ],
    bins: 50,
    xaxis: {
      label: 'I am the X axis',
    },
    yaxis: {
      label: 'I am the Y axis',
    },
    margin: {
      bottom: 55,
    },
    tooltip: {
      formatter: () => {
        console.log('TK');
      },
    },
  };
  const histogram = new Histogram(document.getElementById('histogram'), histogramConfig);

  // Set up 'data update' button for this chart
  document.getElementById('histogram-update').addEventListener('click', (e) => {
    e.preventDefault();
    histogram.updateConfig({
      data: [
        {
          label: 'Ice cream',
          color: '#eb34a1',
          values: Array.from({ length: 500 }, () => Math.floor(Math.random() * 40)),
        },
      ],
    });
  });


  //
  // Joyplot
  //
  futureDate.setDate(futureDate.getDate() + 21);
  const joyplotConfig = {
    data: [
      {
        label: 'Ice cream',
        color: '#ab6516',
        values: [0, 14, 3, 24, 9, 10, 42, 0, 0, 0, 0, 0, 0, 41, 3, 42, 49, 7, 17, 11, 29],
      },
      {
        label: 'Pie',
        color: '#eb34a1',
        values: [0, 27, 49, 25, 25, 39, 44, 33, 9, 20, 47, 35, 0, 0, 0, 0, 31, 9, 10, 19, 1],
      },
      {
        label: 'Cookies',
        color: '#ab6516',
        values: [47, 22, 45, 48, 10, 19, 29, 45, 5, 7, 50, 48, 24, 29, 49, 4, 34, 40, 28, 22, 48],
      },
      {
        label: 'Licorice',
        color: '#eb34a1',
        values: [0, 21, 35, 46, 24, 43, 0, 0, 0, 14, 25, 13, 34, 23, 45, 28, 35, 8, 29, 44, 13],
      },
      {
        label: 'Cheesecake',
        color: '#eb34a1',
        values: [5, 14, 40, 31, 12, 19, 20, 26, 19, 15, 36, 43, 4, 30, 6, 40, 39, 22, 29, 18, 35],
      },
    ],
    xseries: getDates(startDate, futureDate), // To save space, using date generator here
    xaxis: {
      label: 'I am the X axis',
    },
    yaxis: {
      label: 'I am the Y axis',
    },
    margin: {
      top: 50,
      left: 80,
      bottom: 50,
    },
    tooltip: {
      formatter: () => {
        console.log('TK');
      },
    },
  };
  const joyplot = new Joyplot(document.getElementById('joyplot'), joyplotConfig);

  // Set up 'data update' button for this chart
  document.getElementById('joyplot-update').addEventListener('click', (e) => {
    e.preventDefault();
    joyplot.updateConfig({
      data: [
        {
          label: 'Ice cream',
          color: '#ab6516',
          values: Array.from({ length: 21 }, () => Math.floor(Math.random() * 40)),
        },
        {
          label: 'Pie',
          color: '#eb34a1',
          values: Array.from({ length: 21 }, () => Math.floor(Math.random() * 40)),
        },
        {
          label: 'Cookies',
          color: '#ab6516',
          values: Array.from({ length: 21 }, () => Math.floor(Math.random() * 40)),
        },
        {
          label: 'Licorice',
          color: '#eb34a1',
          values: Array.from({ length: 21 }, () => Math.floor(Math.random() * 40)),
        },
        {
          label: 'Cheesecake',
          color: '#eb34a1',
          values: Array.from({ length: 21 }, () => Math.floor(Math.random() * 40)),
        },
      ],
    });
  });
};

// Initialize your scripts when the page is ready, or now, if it happens
// to be ready
if (document.readyState === 'loading') {
  document.addEventListener('DOMContentLoaded', init);
} else {
  init();
}
