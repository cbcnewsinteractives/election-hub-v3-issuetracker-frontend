import copy from 'clipboard-copy';
import focusLock from 'dom-focus-lock';

export default {
  init() {
    // Set up share tools popup toggle
    document.querySelector('.ni-header .ni-share-tools__popup-toggle').addEventListener('click', this.handlePopupToggle);
    
    // Handle copy link share tool
    document.querySelector('.ni-header .ni-share-tools__icon--copy-link').addEventListener('click', this.handleCopyLink);

    // Hide the share tools when viewed in-app
    if (URLSearchParams) {
      const URLParams = new URLSearchParams(window.location.search);
      const isAppWebView = Boolean(URLParams.get('webview'));

      if (isAppWebView) {
        document.querySelector('.ni-share-tools').classList.add('important-hide');
      }
    }
  },

  handlePopupToggle(e) {
    e.preventDefault();

    if (this.getAttribute('aria-expanded') === 'true') {
      document.body.classList.remove('ni-share-tools-popup-active');
      this.setAttribute('aria-expanded', 'false');
      focusLock.off(this.parentNode);
    } else {
      document.body.classList.add('ni-share-tools-popup-active');
      this.setAttribute('aria-expanded', 'true');
      focusLock.on(this.parentNode);
    }
  },

  handleCopyLink(e) {
    e.preventDefault();

    copy(this.getAttribute('data-link'));
    this.classList.toggle('active');

    setTimeout(() => {
      this.classList.remove('active');
    }, 3000);
  },
};
