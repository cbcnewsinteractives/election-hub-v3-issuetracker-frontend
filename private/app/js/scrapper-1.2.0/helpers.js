const helpers = {
  getScrollY() {
    return window.scrollY ? window.scrollY : window.pageYOffset;
  },
  getContentPath(type) {
    return env == 'ee' ? `/interactives/content/${type}/` : `${type}/`;
  },
  testTouch() {
    return ('ontouchstart' in window || navigator.maxTouchPoints) === true;
  },
  getRandomId(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
  },
  readSocialMeta() {
    const socialData = {};
    const metaTags = document.getElementsByTagName('meta');

    for (let i = 0; i < metaTags.length; i++) {
      let prop = metaTags[i].getAttribute('property');
      const val = metaTags[i].getAttribute('content');
      let name = metaTags[i].getAttribute('name');

      if (prop && prop.indexOf('og') != -1) {
        prop = prop.replace(':', '_');
        socialData[prop] = val;
      }

      if (name && name.indexOf('twitter') != -1) {
        name = name.replace(':', '_');
        socialData[name] = val;
      }
    }

    return socialData;
  },
  isElementInViewport(elem) {
    if (typeof jQuery === 'function' && elem instanceof jQuery) {
      elem = elem[0];
    }

    const rect = elem.getBoundingClientRect();
    const winHeight = (window.innerHeight || document.documentElement.clientHeight);

    return (rect.top + rect.height) >= 0;
  },
  getParameterByName(name, url) {
	    if (!url) url = window.location.href;
	    name = name.replace(/[\[\]]/g, '\\$&');
	    const regex = new RegExp(`[?&]${name}(=([^&#]*)|&|#|$)`);
	        const results = regex.exec(url);
	    if (!results) return null;
	    if (!results[2]) return '';
	    return decodeURIComponent(results[2].replace(/\+/g, ' '));
  },
  removeParamByName(key, url) {
    if (!url) url = window.location.href;
	    let rtn = url.split('?')[0];
	        let param;
	        let params_arr = [];
	        const queryString = (url.indexOf('?') !== -1) ? url.split('?')[1] : '';
	    if (queryString !== '') {
	        params_arr = queryString.split('&');
	        for (let i = params_arr.length - 1; i >= 0; i -= 1) {
	            param = params_arr[i].split('=')[0];
	            if (param === key) {
	                params_arr.splice(i, 1);
	            }
	        }
	    	if (params_arr.length > 0) {
	    		rtn = `${rtn}?${params_arr.join('&')}`;
	    	}
	    }
	    return rtn;
  },
  addParam(key, val, url) {
    if (!url) url = window.location.href;
	    let rtn = url.split('?')[0];
	        let param;
	        let params_arr = [];
	        const queryString = (url.indexOf('?') !== -1) ? url.split('?')[1] : '';
	    if (queryString !== '') {
	        params_arr = queryString.split('&');
	        for (let i = params_arr.length - 1; i >= 0; i -= 1) {
	            param = params_arr[i].split('=')[0];
	            if (param === key) {
	                params_arr.splice(i, 1);
	            }
	        }
	        params_arr.push(`${key}=${val}`);
	    	if (params_arr.length > 0) {
	    		rtn = `${rtn}?${params_arr.join('&')}`;
	    	}
	    } else {
	    	rtn = `${rtn}?${key}=${val}`;
	    }

	    return rtn;
  },
  getCookie(cname) {
	    const name = `${cname}=`;
	    const decodedCookie = decodeURIComponent(document.cookie);
	    const ca = decodedCookie.split(';');
	    for (let i = 0; i < ca.length; i++) {
	        let c = ca[i];
	        while (c.charAt(0) == ' ') {
	            c = c.substring(1);
	        }
	        if (c.indexOf(name) == 0) {
	            return c.substring(name.length, c.length);
	        }
	    }
	    return '';
  },
  getSocialParams() {
    const data = {};
    if (location.pathname) {
      data.story_path = location.pathname;
    }
    data.cbc_unit = document.querySelector('body').getAttribute('data-unit');
    if (helpers.getCookie('cbc_plus')) {
      data.visitor_id = helpers.getCookie('cbc_plus');
    } else if (helpers.getCookie('cbc_visitor')) {
      data.visitor_id = helpers.getCookie('cbc_visitor');
    } else {
      data.visitor_id = null;
    }
    if (location.href.includes('//newsinteractives.cbc.ca/longform') || location.href.includes('craftlocal:8888/longform')) {
      data.cbc_platform = 'C';
      data.story_id = document.querySelector('body').id.replace('entry', '');
    } else if (location.href.includes('//newsinteractives.cbc.ca/craft') || location.href.includes('craftlocal:8888/craft')) {
      data.cbc_platform = 'C';
      data.story_id = document.querySelector('body').id.replace('entry', '');
    } else if (location.href.includes('cbc.ca/news2/interactives/') || location.href.includes('//newsinteractives.cbc.ca/')) {
      data.cbc_platform = 'O';
      data.story_id = '';
    } else if (location.href.includes('//newsinteractives.cbc.ca/')) {
      data.cbc_platform = 'S';
      data.story_id = null;
    } else if (location.href.match(/\d\.\d{6,9}/)) {
      data.cbc_platform = 'P';
      data.story_id = location.href.match(/\d\.\d{6,9}/)[0];
    } else {
      data.cbc_platform = 'U';
      data.story_id = null;
    }
    return data;
  },
};

export default helpers;
