/* eslint-disable */

import _ from 'underscore';
import SweetScroll from 'sweet-scroll';

let circleElement;
let pageHeight = 100000;
let goingToTop = false;
let atTop = true;
let dashOffset = 195; // Initial stroke-dashoffset of the svg circle

function getScrollPercent() {
  var h = document.documentElement,
      b = document.body,
      st = 'scrollTop',
      sh = 'scrollHeight';
  return (h[st]||b[st]) / ((h[sh]||b[sh]) - h.clientHeight);
}

export default {
  handleScroll: function() {
    if (!goingToTop) {
      const percentScrolled = getScrollPercent();
      const newOffset = Math.max(0, (1 - percentScrolled) * dashOffset);
      circleElement.style.strokeDashoffset = newOffset;

      if (atTop && window.pageYOffset > window.innerHeight * 1.5) {
        atTop = false;
        document.querySelector('.ni-back-to-top').classList.add('active');
      } else if (!atTop && window.pageYOffset < window.innerHeight) {
        atTop = true;
        document.querySelector('.ni-back-to-top').classList.remove('active');
      }
    }
  },
	init: function(issue) {
    circleElement = document.querySelector('.ni-back-to-top svg.circle circle');

    // Bail of the element doesn't exist
    if (!circleElement) {
      return;
    }

    // dashOffset = circleElement.getTotalLength();
    pageHeight = document.body.scrollHeight;
    window.addEventListener('resize', () => {
      pageHeight = document.body.scrollHeight;
    })

    // Set the page height again after a couple seconds after page load
    setTimeout(() => {
      pageHeight = document.body.scrollHeight;
    }, 2000);

    // Call the scroll handler at most once every 100ms
    window.addEventListener('scroll', _.throttle(this.handleScroll, 100));

    // Smooth scroll to top on scroll
    const scroller = new SweetScroll({ duration: 300, easing: 'linear' });
    document.querySelector('button.ni-back-to-top').addEventListener('click', (event) => {
      event.preventDefault();
      scroller.to(0);
      goingToTop = true;
      circleElement.style.strokeDashoffset = dashOffset;

      setTimeout(() => {
        goingToTop = false;
        const logo = document.querySelector('.ni-header__logo')
        if (logo) {
          logo.focus();
        }
      }, 400)
    });
	}
}
