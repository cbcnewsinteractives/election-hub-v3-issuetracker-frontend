// Fetch and render a list of related links based on the pubqueue id set as data-qid on #section-related
export default {
  init: function init() {
    const relatedCont = document.getElementById('section-related');

    if (!relatedCont) {
      console.log('No container found for related items.');
      return;
    }

    function renderLinks() {
      const resp = JSON.parse(this.responseText);
      const data = resp.contentlist.contentitems;
      let markup = '';

      // Extract relevant content from the json response and render
      data.slice(0, 4).forEach((link) => {
        const headline = link.headline ? link.headline : link.title;
        const image300 = link.headlineimage.derivatives['16x9_300'].fileurl;
        const image620 = link.headlineimage.derivatives['16x9_620'].fileurl;
        const url = link.url;

        // build related item and append to existing markup
        markup += `<a class='story-link' href='${url}'>
          <div class='story-image'>
            <img
              srcset='${image300} 300w,
                  ${image620} 620w'
              sizes='(min-width: 640px) 50vw, (min-width: 1025px) 25vw, 90vw'
              src='${image620}'
              alt='' />
          </div>
          <div class='story-text'>
            <h3 class='story-headline'>${headline}</h3>
          </div>
        </a>`;
      });

      // insert markup into related content section
      relatedCont.querySelector('.articles-list').innerHTML = markup;
    }

    const pubId = relatedCont.getAttribute('data-qid');

    const dataUrl = `https://www.cbc.ca/json/cmlink/${pubId}`;
    const req = new XMLHttpRequest();
    req.addEventListener('load', renderLinks);
    req.open('GET', dataUrl);
    req.send();
  },
};
