
import $ from 'jquery';


export default {
	init: function() {


  $('li#polls').on('click', function() {
     window.location = "https://newsinteractives.cbc.ca/elections/poll-tracker/canada/"; 
  });
  $('li#ask').on('click', function() {
     window.location = "https://www.cbc.ca/news/canada/topic/Subject/Ask%20CBC"; 
  });
  $('li#compass').on('click', function() {
     window.location = "https://votecompass.cbc.ca/canada"; 
  });
  $('li#platforms').on('click', function() {
     window.location = "https://newsinteractives.cbc.ca/elections/federal/2021/party-platforms/"; 
  });


var discoveryScroll = $("#discoveryScroll");

var lastScrollLeft = 0;
$("#discoveryScroll").scroll(function(e) {
    var documentScrollLeft = $("#discoveryScroll").scrollLeft();
    if (lastScrollLeft != documentScrollLeft) {

        function between(documentScrollLeft, min, max) {
            return documentScrollLeft >= min && documentScrollLeft <= max;
          }

          if (between(documentScrollLeft, 0, 15)) {

            $('#canada-votes-logo').css({'opacity' : '1',
                                         'width' : '132px',
                                         'margin-left' : '0'});
            
          } else if (between(documentScrollLeft, 16, 31)) {

            $('#canada-votes-logo').css({'opacity' : '.9',
                                         'width' : '130px',
                                         'margin-left' : '1px'});

          } else if (between(documentScrollLeft, 32, 47)) {

            $('#canada-votes-logo').css({'opacity' : '.8',
                                         'width' : '128px',
                                         'margin-left' : '2px'});

          } else if (between(documentScrollLeft, 48, 63)) {

            $('#canada-votes-logo').css({'opacity' : '.7',
                                         'width' : '126px',
                                         'margin-left' : '3px'});

          } else if (between(documentScrollLeft, 64, 79)) {

            $('#canada-votes-logo').css({'opacity' : '.6',
                                         'width' : '124px',
                                         'margin-left' : '4px'});

          } else if (between(documentScrollLeft, 80, 95)) {

            $('#canada-votes-logo').css({'opacity' : '.5',
                                         'width' : '122px',
                                         'margin-left' : '5px'});

          } else if (between(documentScrollLeft, 96, 107)) {

            $('#canada-votes-logo').css({'opacity' : '.4',
                                         'width' : '120px',
                                         'margin-left' : '6px'});

          } else if (between(documentScrollLeft, 108, 115)) {

            $('#canada-votes-logo').css({'opacity' : '.3',
                                         'width' : '118px',
                                         'margin-left' : '7px'});

          } else if (between(documentScrollLeft, 116, 122)) {

            $('#canada-votes-logo').css({'opacity' : '.2',
                                         'width' : '116px',
                                         'margin-left' : '8px'});

          } else if (between(documentScrollLeft, 123, 132)) {

             $('#canada-votes-logo').css({'opacity' : '.1',
                                         'width' : '114px',
                                         'margin-left' : '9px'});

          } else if (documentScrollLeft > 133) {
            $('#canada-votes-logo').css({'opacity' : '0',
                                        'width' : '112px',
                                         'margin-left' : '10px'});
          }

        lastScrollLeft = documentScrollLeft;
    }
});

  }
}