require('@project/styles'); // Tell webpack to process our stylesheets

// Always polyfill Promises and Array.includes for cbc-stats-top.js/cbc-stats-bottom.js
// Other polyfills will be loaded by babel/core-js as they are encountered in your code
import 'core-js/modules/es.array.includes';
import 'core-js/features/promise';

// Vendor libraries
import 'what-input';
import 'highlight.js/styles/dracula.css';

import jQuery from 'jquery';
import $ from 'jquery';
// export for others scripts to use
window.$ = $;
window.jQuery = jQuery;

// Import default Scrapper modules
import backToTop from './scrapper-1.2.0/back-to-top';
import related from './scrapper-1.2.0/related';
import shareTools from './scrapper-1.2.0/share-tools';
import spaceButton from './scrapper-1.2.0/space-button';
import analysisButton from './scrapper-1.2.0/analysis-button';
import discovery from './scrapper-1.2.0/discovery';
import Issues from './components/issues/index.js';
import scrollNav from './components/scroll-nav';
import navDropdown from './components/nav-dropdown';
import intersectionObserverPolyfill from './components/intersection-observer';

import Waypoints from './vendor/noframework.waypoints';


const init = () => {
  // Initialize modules
  related.init();
  backToTop.init();
  shareTools.init();
  spaceButton.init();
  scrollNav.init();
  analysisButton.init();
  discovery.init();


  // Project-specific modules
  const issues = new Issues();
  issues.fetchData()
    .then((data) => {
      issues.setup();
      issues.render();
    });

//so the page has time to render...

setTimeout(function(){ navDropdown.init(); }, 100);
 
};

// Initialize your scripts when the page is ready, or now, if it happens
// to be ready
if (document.readyState === 'loading') {
  document.addEventListener('DOMContentLoaded', init);
} else {
  init();
  
}
