import axios from 'axios';
import _ from 'lodash';


import Nav from './modules/nav';
import MobileNav from './modules/mobile-nav';
import Body from './modules/body';
import Dek from './modules/dek';

class Main {
  constructor() {
    this.config = window.config || {};
    this.cache = {};
    this.sections = {}
  }

  fetchData(force = false) {
    let that = this
    const apiUrl = this.config.urls.apiUrl;

    if (this.cache['data'] && !force) {
      return Promise.resolve(this.cache['data']);
    }

    return axios.get(apiUrl)
      .then(function (response) {
        // root object is response.data, we're only after the data property

        let toFormatDate = response.data.data.changes;

        let toSortIssues = response.data.data.issues;

        //sorting issues by label      
        toSortIssues.sort(function(a, b){
            if(a.label< b.label) return -1;
            if(a.label> b.label) return 1;
            return 0;
        })

        //sorting positions on each issue by party
        var i, j = "";

        for (i in response.data.data.issues) {
 
        for (j in response.data.data.issues[i].positions) {

                  let toSortParties = response.data.data.issues[i].positions;

              //sorting parties by creating an object for comparison
          let order = { "Liberal": 1, "Conservative": 2, "New Democrat": 3, "Bloc Québécois": 4, "Green": 5, "People's Party of Canada": 6 };

          toSortParties.sort(function (a, b) {
              return order[a.party] - order[b.party];
          });

        }
      }

        //formatting dates from ISO on each creation date in change log
        var k = "";

        for (k in response.data.data.changes) {

            let toFormatDates = response.data.data.changes[k].created_at; 
        
            const monthNames = ["January", "February", "March", "April", "May", "June",
              "July", "August", "September", "October", "November", "December"
            ];

            var date = new Date(toFormatDates);
            let formattedDate = monthNames[date.getMonth()+1] + ' '+date.getDate()+ ': ';
            response.data.data.changes[k].created_at = formattedDate;

      }

        that.cache['data'] = response.data.data;  
        return response.data.data;
      })
  }

  getData() {
    return _.cloneDeep(this.cache['data']);
  }

  setup() {
    this.sections.nav = new Nav(this);
    this.sections.mobileNav = new MobileNav(this);
    this.sections.body = new Body(this);
    this.sections.dek = new Dek(this);
  }

  render() {
    for (let [key, val] of Object.entries(this.sections)) {
      this.sections[key].render();
    }
  
  }
}

export default Main;
