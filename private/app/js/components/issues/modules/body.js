import Base from './Base';
import StickyNav from 'components/sticky-nav';
let $ = require('jquery');

class Body extends Base {
  constructor(st) {
    super(st);

    this.id = 'Body';
    this.$el = $('#issues-body');
  }

  template() {
    return require('../views/body')
  }

  afterRender() {
    new StickyNav();
    // You can put code here that will be executed after the template is rendered
  }
}

export default Body;
