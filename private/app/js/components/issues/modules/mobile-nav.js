import Base from './Base';
let $ = require('jquery');

class MobileNav extends Base {
  constructor(st) {
    super(st);

    this.id = 'MobileNav';
    this.$el = $('#issues-nav-dropdown');
  }

  template() {
    return require('../views/mobile-nav')
  }

  afterRender() {
    // You can put code here that will be executed after the template is rendered
  }
}

export default MobileNav;
