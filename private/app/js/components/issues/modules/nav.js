import Base from './Base';
let $ = require('jquery');

class Nav extends Base {
  constructor(st) {
    super(st);

    this.id = 'Nav';
    this.$el = $('#issues-nav');
  }

  template() {
    return require('../views/nav')
  }

  afterRender() {
    // You can put code here that will be executed after the template is rendered
  }
}

export default Nav;
