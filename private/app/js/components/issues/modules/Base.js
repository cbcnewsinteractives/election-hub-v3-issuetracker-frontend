class Base {
  constructor(st) {
    this.config = window.config || {};
    this.st = st;

    // default
    this.id = 'Unknown';
  }

  getTemplateVars() {
    const to_return = {};
    to_return.data = this.st.getData();
    to_return.config = this.config;

    to_return.slugify = function (text) {
      return text ? text.replace(/\s+/g, "-").toLowerCase() : '';
    }

    const config = this.config;

    // trying this out: if a particular tmeplate needs something, support a
    // subTemplateVars = function(to_return){ }
    // that returns a modified to_return
    if (this['getSubTemplateVars'] && typeof this['getSubTemplateVars'] == 'function') {
      to_return = this.getSubTemplateVars(to_return);
    }

    return to_return;
  }

  render() {
    const that = this;
    const tpl = this.template()
    const $el = this.$el;

    const variables = this.getTemplateVars();
    if (this['view_options']) {
      variables.view_options = this.view_options;
    }

    try {
      that.$el.html(tpl(variables))
    } catch (e) {
      console.error(this.id + ': caught error in render: ', e);
    }

    if (that['afterRender'] && typeof that['afterRender'] == 'function') {
      // console.log('afterRender exists here, running', that);
      this.afterRender()
    }

  }
}

export default Base;
