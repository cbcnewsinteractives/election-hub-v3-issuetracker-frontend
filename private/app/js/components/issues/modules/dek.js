import Base from './Base';
let $ = require('jquery');

class Dek extends Base {
  constructor(st) {
    super(st);

    this.id = 'Dek';
    this.$el = $('#dek');
  }

  template() {
    return require('../views/dek')
  }

  afterRender() {

    console.log("rendered");
  
    // You can put code here that will be executed after the template is rendered
  }
}

export default Dek;
