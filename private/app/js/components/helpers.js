let helpers = {
	getScrollY: function(){
		return window.scrollY ? window.scrollY : window.pageYOffset;
	},
	getContentPath: function(type){
		return env == 'ee' ? '/interactives/content/' + type + '/' : type + '/';
	},
	testTouch: function(){
		return true === ('ontouchstart' in window || navigator.maxTouchPoints);
	},
	getRandomId: function(min, max){
		return Math.floor(Math.random() * (max - min)) + min;
	},
	slugify: function(text) {
		return text.replace(/[^\w\s]/g,'').replace(/\s+/g, '-').toLowerCase()
	},
	shuffleArray: function(unsortedArray) {
		return unsortedArray.map((a) => ({sort: Math.random(), value: a})).sort((a, b) => a.sort - b.sort).map((a) => a.value)
	},
	readSocialMeta: function(){
		var socialData = {};
		var metaTags = document.getElementsByTagName('meta');

		for(var i = 0; i < metaTags.length; i++){
			var prop = metaTags[i].getAttribute('property');
			var val = metaTags[i].getAttribute('content');
			var name = metaTags[i].getAttribute('name');

			if(prop && prop.indexOf('og') != -1){
				prop = prop.replace(':', '_');
				socialData[prop] = val;
			};

			if(name && name.indexOf('twitter') != -1){
				name = name.replace(':', '_');
				socialData[name] = val;
			};
		}

		return socialData;
	},
	isElementInViewport: function(elem){
		if (typeof jQuery === 'function' && elem instanceof jQuery){
			elem = elem[0];
		};

		var rect = elem.getBoundingClientRect();
		var winHeight = ( window.innerHeight || document.documentElement.clientHeight );

		return (rect.top + rect.height) >= 0;
	},
	getParameterByName: function(name, url) {
	    if (!url) url = window.location.href;
	    name = name.replace(/[\[\]]/g, "\\$&");
	    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
	        results = regex.exec(url);
	    if (!results) return null;
	    if (!results[2]) return '';
	    return decodeURIComponent(results[2].replace(/\+/g, " "));
	},
	removeParamByName: function(key, url) {
		if (!url) url = window.location.href;
	    var rtn = url.split("?")[0],
	        param,
	        params_arr = [],
	        queryString = (url.indexOf("?") !== -1) ? url.split("?")[1] : "";
	    if (queryString !== "") {
	        params_arr = queryString.split("&");
	        for (var i = params_arr.length - 1; i >= 0; i -= 1) {
	            param = params_arr[i].split("=")[0];
	            if (param === key) {
	                params_arr.splice(i, 1);
	            }
	        }
	    	if (params_arr.length > 0) {
	    		rtn = rtn + "?" + params_arr.join("&");
	    	}
	    }
	    return rtn;
	},
	addParam: function(key, val, url) {
		if (!url) url = window.location.href;
	    var rtn = url.split("?")[0],
	        param,
	        params_arr = [],
	        queryString = (url.indexOf("?") !== -1) ? url.split("?")[1] : "";
	    if (queryString !== "") {
	        params_arr = queryString.split("&");
	        for (var i = params_arr.length - 1; i >= 0; i -= 1) {
	            param = params_arr[i].split("=")[0];
	            if (param === key) {
	                params_arr.splice(i, 1);
	            }
	        }
	        params_arr.push(key+'='+val);
	    	if (params_arr.length > 0) {
	    		rtn = rtn + "?" + params_arr.join("&");
	    	}
	    }else{
	    	rtn = rtn + '?'+key+'='+val;
	    }

	    return rtn;
	},
	getCookie: function(cname) {
	    var name = cname + "=";
	    var decodedCookie = decodeURIComponent(document.cookie);
	    var ca = decodedCookie.split(';');
	    for(var i = 0; i <ca.length; i++) {
	        var c = ca[i];
	        while (c.charAt(0) == ' ') {
	            c = c.substring(1);
	        }
	        if (c.indexOf(name) == 0) {
	            return c.substring(name.length, c.length);
	        }
	    }
	    return "";
	},
	getSocialParams: function(){
		var data = {};
		if(location.pathname){
			data.story_path = location.pathname;
		}
		data.cbc_unit = document.querySelector('body').getAttribute('data-unit');
		if(helpers.getCookie('cbc_plus')){
			data.visitor_id = helpers.getCookie('cbc_plus');
		}else if(helpers.getCookie('cbc_visitor')){
			data.visitor_id = helpers.getCookie('cbc_visitor');
		}else{
			data.visitor_id = null;
		}
		if(location.href.includes("//newsinteractives.cbc.ca/longform") || location.href.includes("craftlocal:8888/longform")){
			data.cbc_platform = "C";
			data.story_id = document.querySelector('body').id.replace("entry","");
		}else if(location.href.includes("//newsinteractives.cbc.ca/craft") || location.href.includes("craftlocal:8888/craft")){
			data.cbc_platform = "C";
			data.story_id = document.querySelector('body').id.replace("entry","");
		}else if(location.href.includes("cbc.ca/news2/interactives/") || location.href.includes("//newsinteractives.cbc.ca/")){
			data.cbc_platform = "O";
			data.story_id = "";
		}else if(location.href.includes("//newsinteractives.cbc.ca/")){
			data.cbc_platform = "S";
			data.story_id = null;
		}else if(location.href.match(/\d\.\d{6,9}/)){
			data.cbc_platform = "P";
			data.story_id = location.href.match(/\d\.\d{6,9}/)[0];
		}else{
			data.cbc_platform = "U";
			data.story_id = null;
		}
		return data;
	}
}

export default helpers;
