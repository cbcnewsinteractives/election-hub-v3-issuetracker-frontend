  
import SweetScroll from 'sweet-scroll';

export default {
  init: function() {

  const scroller2 = new SweetScroll({ duration: 300, easing: 'linear', offset: -30});
  const navSide = document.querySelector('.o-nav__side');
  
  navSide.addEventListener('click', (event) => {
  const elements = document.querySelectorAll('.o-nav__side > ul > li');
  const section = event.target.getAttribute('href');
  scroller2.to(section);
  });
}
}