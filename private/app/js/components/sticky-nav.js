import scrollama from 'scrollama';

class StickyNav {
  constructor() {
    // Set up scroll handler
    const scroller = scrollama();
    scroller
      .setup({
        step: 'div#issues-body > section',
        offset: "40px",
      })
      .onStepEnter(response => {
        // response = { element, index, direction }
        this.addActiveClass(response.element.id);
      });

    // setup resize event
    window.addEventListener('resize', scroller.resize);

    // Cache the nav item dom nodes
    this.listItems = document.querySelectorAll('aside nav a');
  }

  addActiveClass(id) {
    // Reset the nav items
    this.listItems.forEach(function (item) {
      item.classList.remove('active');
      item.setAttribute("aria-current", false);
    });

    // Set active nav item
    let activeSelector = document.querySelector(`a[href='#${id}']`).classList = 'active';
    document.querySelector(`a[href='#${id}']`).setAttribute("aria-current", true);
  }
}


export default StickyNav;
