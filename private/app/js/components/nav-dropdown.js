import h from './helpers';
import Waypoints from '../vendor/noframework.waypoints';
import SweetScroll from 'sweet-scroll';


export default {
  init: function(issue) {
    const nav = document.querySelector('.o-nav__dropdown');
    // Bail of the nav doesn't exist
    if (!nav) {
      return;
    }

    const dropdown = nav.querySelector('select');

    // Handle top stickiness
    const headerHeight = document.querySelector('header').clientHeight;
    const labelHeight = nav.querySelector('label').clientHeight
    const offsetPadding = 16;
    const navHeight = dropdown.clientHeight + (offsetPadding * 2)
    const waypoint = document.querySelector('.o-nav__dropdown-waypoint')

    console.log(waypoint.getBoundingClientRect().top);

    const observer = new IntersectionObserver((intersection) => {
      if (waypoint.getBoundingClientRect().top < headerHeight + navHeight) {
        nav.classList.add('stuck')
      } else {
        nav.classList.remove('stuck')
      }
    }, { rootMargin: '-' + (headerHeight + navHeight) + 'px' });
    observer.observe(waypoint);

    // Handle section waypoints (update the dropdown when scrolling to a section)

    // THIS IS WHERE THE TROUBLE LIES!

    const issueRows = document.querySelectorAll('.issue-heading-container');

    console.log(issueRows);


    for (let i = 0; i < issueRows.length; i++) {
      const issue = issueRows[i].id;

      new Waypoint({
        element: document.getElementById(issue),
        handler: function(direction) {
          if (direction == 'down') {

            dropdown.selectedIndex = i;
          } else if (i > 0) {
            dropdown.selectedIndex = i - 1;
          }
        },
        offset: headerHeight + navHeight + offsetPadding
      })
    }
  


    // Handle smooth scroll on select activation
    const scroller = new SweetScroll({ duration: 300, easing: 'linear', offset: 0 - (headerHeight + navHeight + offsetPadding) });
    dropdown.addEventListener('change', (event) => {


      const section = event.target.value;
      scroller.to('#' + section);
    });
  }
}
