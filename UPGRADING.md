# Upgrading

This document can contain common fixes/pitfalls/etc for updating:
* Scrapper
* Legacy projects
* Whatever

## Build Errors

```
Error: spawn cbc-web-prerender ENOENT                                                                                                                                                             
     at Process.ChildProcess._handle.onexit (internal/child_process.js:267:19)                                                                                                                     
     at onErrorNT (internal/child_process.js:469:16)                                                                                                                                          
     at processTicksAndRejections (internal/process/task_queues.js:84:21)                                                                                                                          
 ```

Run into something like this? Try this:
In your webpack.config.js, look for something like:
```
onBuildEnd: []
    ...
    'cbc-web-prerender -p=dist -f=true',
    ...
]
```
Replace the cbc-web-prerender line with:
```
'/cbc/core/app/php/bin/cbc-web-prerender -p=dist -f=true',
```
